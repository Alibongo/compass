import React from 'react'
import Link from 'gatsby-link'

import '../assets/css/main.css'
import Header from '../components/header/Header'
import About from '../components/about/About'
import PipeSection from '../components/pipeline/PipeSection'
import VideoContent from '../components/videoContent/VideoContent'
import Partners from '../components/partners/Partners'
import Consortium from '../components/consortium/Consortium'
import Agrosavia from '../components/agrosavia/Agrosavia'
import JoinUs from '../components/joinUs/JoinUs'
import Footer from '../components/Footer/Footer'
const IndexPage = () => (
  <div>
    <Header />
    <About />
    <PipeSection />
    <VideoContent />
    <Partners />
    <Consortium />
    <JoinUs />
    <Footer />
  </div>
)

export default IndexPage
