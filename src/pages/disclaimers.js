import React from 'react'
import Link from 'gatsby-link'

import Header2 from '../components/header/Header2'
import About from '../components/about/About'
import PipeSection from '../components/pipeline/PipeSection'
import VideoContent from '../components/videoContent/VideoContent'
import Partners from '../components/partners/Partners'
import Agrosavia from '../components/agrosavia/Agrosavia'
import JoinUs from '../components/joinUs/JoinUs'
import Footer from '../components/Footer/Footer'


import '../assets/css/main.css'

const Disclaimers = () => (
  <div>
    <div>
      <Header2 />
      <Agrosavia />
      <Footer />
    </div>
  </div>
)

export default Disclaimers
