import React from 'react'
import './videoContent.css'

const VideoContent = () => (
  <section id="videoContent">
    <div className="row">
    <div className="twelve columns">
      <h2>Our Background</h2>
      <p>Learn more about the GHRU, who we are, and what we’re doing to tackle antimicrobial resistance.</p>
      <div className='embed-container'>
      <iframe src="https://player.vimeo.com/video/270372271" width="640" height="360" frameBorder="0" data-progress="true" data-seek="true" webkitallowfullscreen="true" mozallowfullscreen="true" allowFullScreen></iframe>
      </div>
      </div>
    </div>
  </section>

)

export default VideoContent