import React from 'react'
import about from '../../assets/images/about2.png'
import  './about.css'

const About = () => (
  <section id="about">

    <div className= "content-margin">
      <div className="about-wrapper">
        <div className= "img-wrapper">
          <img className="aboutUs" src={about} alt="illustration" />
        </div>
      <div className="content">
        <h2>We are:</h2>
        <ul>
        <li><strong>Building</strong><br className= 'about-break'/> genomic surveillance capacity in low and middle-income countries</li>
        <li><strong>Growing</strong><br className= 'about-break'/> a global network of labs with AMR expertise</li>
        <li><strong>Generating</strong><br className= 'about-break'/> actionable data</li>
        <li><strong>Supporting </strong><br className= 'about-break'/> public health programmes</li>
        </ul>
      </div>
    </div>
  </div>
  </section>
)

export default About
