import React from 'react'
import PiDetails from '../../components/piDetails/PiDetails'
import MapPanel from '../../components/mapPanel/MapPanel'

import './partners.css'
import './videoContent.css'


const Partners = () => (
    <section id="partners">
      <MapPanel />
      <div className="row">
          <div className="twelve columns">
            <h2 className="partners-title">Our Partners</h2>
          </div>
      </div>
      <PiDetails />
        <div className="partners-video-container">
          <div className="row">
              <div className="twelve column partners-video">
                <h2>Meet Our Partners</h2>
                <p>Learn more about how we envisage developing and learning from our international collaboration.</p>
                <div className='embed-container'>
                  <iframe src="https://player.vimeo.com/video/325261424" width="640" height="360" frameBorder="0" data-progress="true" data-seek="true" webkitallowfullscreen="true" mozallowfullscreen="true" allowFullScreen></iframe>
                </div>
              </div>
          </div>
        </div>
    </section>
   )

export default Partners