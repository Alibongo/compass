import React from 'react'
import ContentPanel from '../../components/contentPanel/ContentPanel'
import movie from '../../assets/movie_GHRU.mp4'
import  './banner.css'

const Banner = () => (
  <div className="banner">
    <video src={ movie } autoPlay muted loop></video>
    <ContentPanel title="NIHR Global Health Research Unit">
      Genomic Surveillance of Antimicrobial Resistance
    </ContentPanel>
  </div>
)

export default Banner
