import React from 'react'
import './scrollDown.css'

import {FaChevronCircleDown} from 'react-icons/lib/fa'

const ScrollDown = () => (
    <p className="scrolldown">
        <a className="smoothscroll" href="#about">
            <FaChevronCircleDown />
        </a>
    </p>
)

export default ScrollDown