import React from 'react'
import footer from './footer.css'

import NIHR from '../../assets/images/logos/NIHR-white.svg'
import CGPS from '../../assets/images/logos/CGPS-white.svg'

const Footer = () => (

<section id="footer">

<div className='footer-grid-container'>
     <div className='footer-grid'>
        <div className='logo-grid'>
           <img className='logo-grid-item' src={NIHR}/>
        </div>
        <div className='copyright'>© Global Health Research Unit 2019</div>
     </div>
  </div>
</section>
)

export default Footer