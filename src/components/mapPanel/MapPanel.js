import React from 'react'
import map_marks from '../../assets/images/mapWhite.png'
import './mapPanel.css'

const MapPanel = () => (
    <div className="mapPanel">
        <div className = "row">
            <div className="twelve columns">
                <h2> Our Locations</h2>
                <p>To achieve global surveillance, we’ve established units in the UK, Colombia, India, Nigeria and The Philippines.</p>
                <div className="three columns map-list-container">
                    <hr/>
                    <ul className="large map-list">
                        <li><strong>Colombia</strong></li>
                        <li><strong>India</strong></li>
                        <li><strong>Philippines</strong></li>
                    </ul>
                    <hr/>
                </div>
                <div className="six columns map-item">
                  <img alt="" src= {map_marks} className="mapMarks" />
                </div>
                <div className="three columns map-list-container">
                    <hr/>
                    <ul className="large map-list">
                        <li><strong>Nigeria</strong></li>
                        <li><strong>UK</strong></li>
                        <li><strong>WHONET</strong></li>
                    </ul>
                    <hr/>
                </div>
            </div>
        </div>
    </div>
    )
  export default MapPanel
