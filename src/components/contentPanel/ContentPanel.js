import React from 'react'
import PropTypes from 'prop-types'
import contentPanel from './contentPanel.css'

const ContentPanel = (props) =>(
    <div className="banner-text content-margin">
        <h1>{props.title}</h1>
        <h2>{props.children}</h2>
        <p>Bridging genomics, microbiology and big data to tackle global Antimicrobial Resistance (AMR)</p>
    </div>
)

ContentPanel.propTypes ={
    title: PropTypes.string,
    children: PropTypes.string
}

export default ContentPanel
