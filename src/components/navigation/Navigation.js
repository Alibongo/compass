import React from 'react'
import Link from 'gatsby-link'

import GHRU from '../../assets/images/logos/logo_low.png'
import './navigation.css'

const Navigation = () => (
    <div id="nav-wrap">
        <img className="ghru-logo" src={GHRU} />
        <input id="nav"type="checkbox"/>
        <label htmlFor="nav"></label>
        <nav>
            <li><Link to="/" className="smoothscroll" href="#home">Home</Link></li>
            <li><Link to="/#about" className="smoothscroll" href="/#about">About</Link></li>
            <li><Link to="/#pipe" className="smoothscroll" href="/#pipe">Pipeline</Link></li>
            <li><Link to="/#partners" className="smoothscroll" href="#partners">Partners</Link></li>
            <li><Link to="/#consortium" className="smoothscroll" href="#consortium">Consortium</Link></li>
            <li><Link to="/#videoContent" className="smoothscroll" href="#videoContent">Background</Link></li>
            <li><Link to="/#joinUs"className="smoothscroll" href="#joinUs">Join Us</Link></li>
        </nav>
    </div>
)

export default Navigation
