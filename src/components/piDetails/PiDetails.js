import React from 'react'
import PiPanel from '../piPanel/PiPanel'
import ravi from '../../assets/images/partners/ravi.jpg'
import david from '../../assets/images/partners/david.jpg'
import pilar from '../../assets/images/partners/pilar.jpg'
import iruka from '../../assets/images/partners/iruka.jpg'
import celia from '../../assets/images/partners/celia.jpg'
import stelling from '../../assets/images/partners/john.jpg'

import './piDetails.css'

const PiDetails= () => (
  <div className="content-margin">
    <div id="partners-wrapper">
        <PiPanel
          imageSource={ravi}
          lead="Prof. K.L. Ravikumar"
          jobTitle="Chief of Central Research Laboratory"
          institution="KIMS Hospital and Research Centre, Bangalore"
          piName="India"
          description= {`Established and leads a network of 125+ laboratories and hospitals in India.\n
          Developed molecular methods to identify and type S.pneumoniae and culture clinical samples.\n
          Infection Control Committee Chairman, Sentinel Surveillance Programme member and Advisory Board member on antimicrobial resistance`}
          />
        <PiPanel
          imageSource={celia}
          lead="Dr. Celia Carlos"
          jobTitle="Deputy Director"
          institution="Research Institute for Tropical Medicine"
          piName="Philippines"
          description={`Paediatric infectious disease specialist.\n
          Expanded sentinel sites in the Philippines.\n
          President of the Paediatric Infectious Disease Society, she founded the Alliance for Prudent Use of Antibiotics, and represented the Philippines to the World Health Organization, Global Salmonella Surveillance, and WHO Gonococcal Resistance Surveillance Programme in the Western Pacific.`}
          />
        <PiPanel
          imageSource={iruka}
          lead="Prof. Iruka Okeke"
          jobTitle="Professor of Pharmaceutical Microbiology"
          institution="University of Ibadan"
          piName="Nigeria"
          description={`Consultant to the Nigerian Centre for Disease Control, Africa CDC, WHO and others.\n
          On the Surveillance and Epidemiology of Drug Resistant Infections Consortium advisory board of the Wellcome Trust.\n
          Researches antimicrobial resistance, molecular epidemiology, pathogenesis, E. coli, enteric bacteria, applications and uptake of microbiology in Africa; advocating antimicrobial resistance containment.`}
          />
        <PiPanel
          imageSource={pilar}
          lead="Dr Pilar Donado-Godoy"
          jobTitle="Lead Investigator"
          piName="Colombia"
          institution="Colombian Agriculture Research Corporation"
          description={`Public health veterinarian and epidemiologist.\n
          Founder of Colombia’s Programme for Integrated Surveillance of Antimicrobial Resistance - an international network establishing baselines for antimicrobial resistance of zoonotic bacteria.\n
          Works with the Pan American Health Organization of the WHO.`}
          />
        <PiPanel
          imageSource={stelling}
          lead="Dr John Stelling"
          jobTitle="Co-Director of WHONET"
          institution="Brigham and Women's Hospital, Boston"
          piName="WHONET"
          description={`Dr. John Stelling is an infectious disease specialist in Boston, Massachusetts.\n
          He received his medical degree from Johns Hopkins Medical School and has a Master's in Public Health from (MIT) Massachussett's Institute of Technology.\n
          Dr. Stelling has been in practice for 29 years.`}
          />
          <PiPanel
          imageSource={david}
          lead="Prof. David Aanensen"
          jobTitle="Director"
          institution="Centre for Genomic Pathogen Surveillance"
          piName="UK"
          description={`Senior Group Leader at Big Data Institute, University of Oxford.\n
          Within CGPS, provides data and tools for local and international utility, focusing on antimicrobial resistance and genomic surveillance.\n
          Examining pathogen surveys, David contextualises WGS datasets and population structure. He identifies important high-risk clones, risk assessment and management.`}
          />
      </div>
    </div>
)

export default PiDetails