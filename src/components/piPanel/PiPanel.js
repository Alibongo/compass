import React from 'react'
import classnames from 'classnames'
import './piPanel.css'

const PiPanel= ({ className, imageSource = "imageSource", piName = "PI Name", lead ="Name", jobTitle = "jobTitle", institution = "institution", description = "Lorem Ispum", href = "#modal-01" }) => (
    <div className={classnames(className,"partners-item six columns")}>
        <div className="head">
            <img id="imageSource" alt="Partner Photo" src={imageSource} />
        </div>
            <div className ="partners-content">
                <h2>{lead}</h2>
                <h3 className="jobTitle">{jobTitle}</h3>
                <h4 className="institution">{institution}</h4>
                <h4 className="piName">{piName}</h4>
                {description && <p className="displayLinebreak">{description}</p>}
            </div>
    </div>
)

export default PiPanel



