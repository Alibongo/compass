import React from 'react'
import Headroom from 'react-headroom'
import Navigation from '../navigation/Navigation'
import Banner from '../banner/Banner'
import ContentPanel from '../../components/contentPanel/ContentPanel'
import ScrollDown from '../scrollDown/ScrollDown'
import  './header2.css'

const Header = () => (
  <header id="home2">
  <Headroom>
    <Navigation />
  </Headroom>
  <ScrollDown />
  </header>
)

export default Header