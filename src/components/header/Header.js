import React from 'react'
import Headroom from 'react-headroom'
import Navigation from '../navigation/Navigation'
import Banner from '../banner/Banner'
import ScrollDown from '../scrollDown/ScrollDown'
import  './header.css'

const Header = () => (
  <header id="home">
  <Headroom>
    <Navigation />
  </Headroom>
  <Banner />
  <ScrollDown />
  </header>
)

export default Header