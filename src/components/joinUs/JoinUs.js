import React from 'react'
import Link from 'gatsby-link'

import { FaEnvelopeO  } from'react-icons/lib/fa'
import './joinUs.css'

const JoinUs = () => (
 <section id="joinUs">
    <div className="row">
      <div className="six columns">
        <h2>Join Us</h2>
        <p className="join-us-paragraph"> If you are interested in joining our network of sentinel sites for genomic surveillance of AMR, please contact us via details below.</p>
        <h2>Contact Details</h2>
            <p className="join-us-paragraph">
              <span>NIHR Global Health Research Unit</span>
              <br />
              <span>Centre for Genomic Pathogen Surveillance</span>
              <br />
              <span>Wellcome Genome Campus</span>
              <br />
              <span>Hinxton, Cambridge</span>
              <br/>
              <span>CB10 1SA UK</span>
              <br />
              <span>+44(0)1223 495356</span>
              <br />
              <span>info@ghru.net</span>
            </p>
            <div className="email-us">
              <p>
                <a href="mailto:info@ghru.net?subject=Website Query" className="button"><FaEnvelopeO /> Email Us</a>
              </p>
          </div>
          </div>
          <div className="six columns">
          <h2>Partner Institutions</h2>
          <ul>
            <li>KIMS Hospital and Research Centre, Bangalore</li>
            <li>Research Institute for Tropical Medicine</li>
            <li>University of Ibadan</li>
            <li>Colombian Agriculture Research Corporation</li>
            <li>WHONET</li>
            <li>Centre for Genomic Pathogen Surveillance</li>
          </ul>
          <h2>Disclaimers</h2>
          <ul>
            <li><Link to="/disclaimers/#agrosavia" className="disclaimer-links">NIHR Disclaimer</Link></li>
            <li><Link to="/disclaimers/#agrosavia" className="disclaimer-links">Agrosavia Terms of Use</Link></li>
          </ul>
      </div>
    </div>
  </section>
)

export default JoinUs
