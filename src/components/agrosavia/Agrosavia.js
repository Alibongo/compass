import React from 'react'
import Link from 'gatsby-link'
import  './agrosavia.css'

import { FaChevronCircleUp } from "react-icons/lib/fa"

const Agrosavia = () => (
  <section id="agrosavia">
    <div className ="row">
      <div className="twelve columns">
        <h2 className="disclaimers-title">Disclaimers</h2>
          <div className="content">
            <h2 className="sub-header">NIHR Disclaimer&#58;</h2>
              <em>&quot;This project is funded by the National Institute for Health Research &#40;NIHR&#41;. The views expressed are those of the authors andnot necessarily those of the NIHR or the Department of Health and Social Care.&quot;
              </em>
          </div>
          <hr className="line"/>
          <div className="content">
            <h2 className="sub-header">Agrosavia Terms of Data Use&#58;</h2>
              <em>&quot;The intellectual property rights on the biological resource used to generate this data and on the data generated from it  are owned by the Colombian State, as established by the Andean Pact Decision 391 of the Andean Community of Nations, internal regulations and the provisions of the Colombian Ministry of Environment and Sustainable Development. This data may be used for scientific purposes, to generate knowledge and explain natural phenomena and processes. It shall not be used for activities relating to biological prospecting, industrial application or commercial exploitation without the prior written consent of the Colombian State.  If you have any queries, you are advised to contact Wellcome Sanger Institute in the first instance.&quot;
              </em>
          </div>
          <hr className="line"/>
        <div className="return-homepage">
                <p>
                <Link to="/">Back to the homepage</Link>
                </p>
        </div>
      </div>
    </div>
  </section>
)

export default Agrosavia
