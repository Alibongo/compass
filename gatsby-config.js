module.exports = {
  siteMetadata: {
    title: "NIHR Global Health Research Unit - Genomic Surveillance of Antimicrobial Resistance",
    author: "Ali Molloy",
    description: "GHRU website"
  },
  pathPrefix: "/ghru",
  plugins: [
    {
      resolve:`gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [require("tailwindcss")],
      }
  },


    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`
      }
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`roboto`, `Saira Semi Condensed`]
      }
    },
  {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-122883866-1",
        // tracking script in the head instead of the body
        head: false,
        // optional param
        anonymize: true,
        // optional param
        respectDNT: true,
        // Avoid sending pageview hits from custom paths
        exclude: ["/preview/**", "/do-not-track/me/too/"],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`
  ]
};
